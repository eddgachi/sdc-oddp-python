# a module is a file containing Python definitions and statements
# that can be imported and used in another Python file

# A package is a collection of related modules that are grouped together in a directory/folder.

# from intro_to_python import my_second_module
# from intro_to_python.my_second_module import multiply_numbers

# name = "John Doe"

# multiplied_numbers = my_second_module.multiply_numbers(4, 5)
# print(multiplied_numbers)

import math
from math import cos, sqrt

x = 16
y = math.sqrt(x)
z = sqrt(x)
a = cos(x)

print(y)
