x = 5  # x is my variable name and 5 is my number (int - whole number)
y = 10

z = 3.14  # floating-point number
d = 2.17

c = 3 + 4j  # complex number
d = 2 + 6j

# Arithmetic operations

print(type(z))  # gives us the data type of a variable
print(int(z))  # converts other data types to integers
print(float(x))  # converts to a float
print(complex(z, x))  # converts to a complex number
print(x + y)
print(y - x)
print(x * y)
print(x / y)
print(x % y)

name = "Alice"  # string
location = "Nairobi"
alice_data = name + " from " + location
string4 = "PYTHON"
string5 = "programming"
print(string4.lower())
print(string5.upper())
print(alice_data)
print(string4 * 4)
# slicing - upto but not including
print(string5[3:8])  # we count from zero
print(string4[0:2])
print(string5[3:])
print(string5[:4])
print(len(string5))
print(type(x))
print(str(x))

numbers_list = [71, 32, 83, 14, 900]
fruits_list = ["apple", "banana", "melon", "orange"]
list_data = [80, 5.67, "Hello"]
# print(fruits_list[1])
# print(fruits_list[0])
# print(fruits_list[1:3])
fruits_list.append("cherry")
# print(fruits_list)
# fruits_list.remove("apple")
# print(fruits_list)
# print(fruits_list)
# print(len(fruits_list))
# print(numbers_list)
# fruits_list.sort()
# numbers_list.sort()
# print(fruits_list)
# print(numbers_list)
# print(min(numbers_list))
# print(max(numbers_list))

coordinates = (43, 77)
colors = ("red", "green", "blue")
tuple_data = (54, "Hello", 4.32)
# print(colors[1])

age = {"John": 43, "Jane": 41, "Jim": 44}  # keys must always be strings
dictionary_data = {  # key-value pairs
    "name": "Mary",  # name is the 'key' and Mary is the 'value'
    "age": 34,
    "height": 5.9,
    "is_active": False,
}
dictionary_data["gender"] = "Female"

# print(dictionary_data["name"])
dictionary_data["age"] = 33
# print(dictionary_data)

is_active = True
is_online = False

result = None
# name = input("What is your name?")
# age = input("What is your age?")
# print("Your name is " + name + " and you are " + age + " years old!")  # string concatenation
# print("Your name is {} and you are {} years old!".format(name, age))

# arithmetic -> mathematical operations
# comparison -> comparing one item to another
# assignment -> giving something a value
# logical operators -> add logic

# Arithmetic Operators: +, -, *, /, %, //, **
# a = 10
# b = 20
# print("a + b =", a + b)  # addition
# print("a - b =", a - b)  # subtraction
# print("a * b =", a * b)  # multiplication
# print("a / b =", a / b)  # division
# print("a % b =", a % b)  # remainder of the division
# print("a ** b =", a**b)  # remainder of the division

# Comparison Operators: ==, !=, >, <, >=, <=
# print("a == b", a == b)  # equal
# print("a != b", a != b)  # not equal to
# print("a > b", a > b)  # greater than
# print("a < b", a < b)  # less than
# print("a >= b", a >= b)  # greater than OR equal to
# print("a <= b", a <= b)  # less than OR equal to
# a = 10
# b = 20
# Assignment Operators: =, +=, -=, *=, /=, %=, //=, **=
# equal
# a = b
# print("a =", a)
# a = a + b
# add and assign
# a += b
# print("a =", a)
# subtract and assign
# a -= b
# print("a =", a)
# multiply and assign
# a *= b
# print("a =", a)
# # divide and assign
# a /= b
# print("a =", a)

# # modulus and assign
# a %= b
# print("a =", a)

# # exponential and assign
# a **= b
# print("a =", a)

# Logical Operators: and, or, not
# and - both conditions must be True
# or - at least one condition must be True
# not - invert the boolean value

# a = True
# b = False
# print("a and b is", a and b)  # and
# print("a or b is", a or b)  # or
# print(" not is", not b)  # or
is_signed_in = False
user_name = "John"

# Control flow statements in Python are used to
# execute specific code blocks based on certain conditions
# if is_signed_in:  # if is_signed_in is True execute this code
#     print("You are signed in")
# elif is_signed_in is True or user_name == "Mary":
#     print("Welcome John")
# else:
#     print("Please sign in")

# x = 10
# if x < 0:
#     print("x is negative")
# elif x == 0:
#     print("x is zero")
# else:
#     print("x is positive")

# For Loop: It's used to execute a block of code several times

for number in range(10):  # upto but not including
    print("Hello world")

i = 0
while i < 5:
    print(i)
    i += 1
