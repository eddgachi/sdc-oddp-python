# import modules_packages_recap
# from modules_packages_recap import name

# imported_name = modules_packages_recap.name
# print(imported_name)

# print(name)

# Object-oriented programming (OOP) is a programming paradigm
# that uses the concept of "objects" to structure code.
# Python is an object-oriented language, which means that
# it provides support for OOP concepts like classes, objects, inheritance,
# and polymorphism.

#  A class is a blueprint or template for creating objects,
#  which are instances of the class.


# class Person:
#     def __init__(self, name, age, gender):
#         self.name = name
#         self.age = age
#         self.gender = gender

#     def say_hello(self):
#         print("Hello, my name is {} and I am {} years old".format(self.name, self.age))

#     def start_walking(self):
#         print("I am now walking")


# person1 = Person("Edd", 38, "Male")
# print(person1.name)

# person1.say_hello()
# person1.start_walking()

# person2 = Person("Gladys", 41, "Female")
# print(person2.name)

# person2.say_hello()

# create a class called Rectangle. Your __init__ method has the parameters
# width and height. Create a function called area that calculates the area
# of the rectangle. Create a second function called perimeter 2(w + h) that calculates
# the perimeter of the rectangle
# BODMAS


# class Rectangle:
#     def __init__(self, width, height):
#         self.width = width
#         self.height = height

#     def area(self):
#         return self.width * self.height

#     def perimeter(self):
#         return 2 * (self.width + self.height)


# my_rectangle = Rectangle(40, 60)
# print("The area is {}".format(my_rectangle.area()))
# print("The perimeter is {}".format(my_rectangle.perimeter()))

# Inheritance allows you to create a new class based on an existing class,
# inheriting the attributes and methods of the parent class.

# This can be useful for creating more specialized classes
# without duplicating code.


# class Animal:
#     def __init__(self, name):
#         self.name = name

#     def make_sound(self):
#         pass


# class Dog(Animal):
#     def make_sound(self):
#         print("Woof!")


# class Cat(Animal):
#     def make_sound(self):
#         print("Meow!")


# dog = Dog("Ralph")
# print(dog.name)
# dog.make_sound()

# cat = Cat("Sewi")
# print(cat.name)
# cat.make_sound()

# Polymorphism allows you to use objects of different classes
# in the same way, as long as they have the same methods
# with the same arguments

# animals = [Dog("Johnny"), Cat("Mary"), Dog("Mike"), Cat("Martin")]

# for animal in animals:
#     animal.make_sound()  # example of polymorphism
#     animal.name


class Shape:
    def area(self):
        pass


class Rectangle(Shape):
    def __init__(self, length, width):
        self.length = length
        self.width = width

    def area(self):
        return self.length * self.width


class Circle(Shape):
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return 3.14159 * self.radius**2


shapes = [Rectangle(5, 10), Circle(7)]
for shape in shapes:
    print(shape.area())
