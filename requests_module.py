# request ->
# <- response
# https://pypi.org/
# GET Request - requesting some data
# POST Request - sending data
# Status Code - 200 - everything is ok

import requests

response = requests.get("https://jsonplaceholder.typicode.com/")
if response.status_code == 200:
    print("The request was successful")
else:
    print("The request was not successful")
# print(response.status_code)
# print(response.text)

# response = requests.get("https://jsonplaceholder.typicode.com/albums/1")
# print(response.text)

# response = requests.post(
#     url="https://jsonplaceholder.typicode.com/",
#     data={
#         "userId": 1,
#         "userName": "john",
#         "userEmail": "john@mail.com",
#         "password": "qwerty123",
#     },
# )

# print(response.status_code)
# print(response.text)
