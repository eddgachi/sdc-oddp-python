# Write a program that takes a string as input from the user
# and checks if it's a palindrome or not using functions.
# mADaM - madam


def is_palindrome(word):
    word = word.lower()
    print("word to check: ", word)
    print("word in reverse: ", word[::-1])
    return word == word[::-1]


word_to_check = input("Enter the word you want to check: ")

if is_palindrome(word=word_to_check):
    print("The word {} is a palindrome".format(word_to_check))
else:
    print("The word {} is not a palindrome".format(word_to_check))

first_name = "Edd"
second_name = "edd"
first_number = 2
second_number = 2.0

if first_number == second_number:
    print("The numbers are the same")
else:
    print("The numbers are not the same")
