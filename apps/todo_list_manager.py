# To-Do List Manager using Lists and Dictionaries

# Initialize an empty list to store tasks
tasks = []
# fruits = ['apple', 'banana', 'mango']

# 1. apple
# 2. banana
# 3. mango

# 2 - 1 = 1 -> index of banana
# 3 - 1 = 2 -> index of mango
# task['taskItem1', 'taskItem2]


# Display the available options
def display_options():
    print("To-Do List Manager")
    print("--------------------")
    print("1. Add Task")
    print("2. Delete Task")
    print("3. View All Tasks")
    print("4. Mark Task As Complete")
    print("5. Exit")


# Add a new task to the list
def add_task():
    task_name = input("Enter task name: ")
    tasks.append({"name": task_name, "completed": False})
    print("Task added successfully! \n")


# Delete a task from the list
def delete_task():
    task_number = None
    try:
        # code that may raise an exception
        task_number = int(input("Enter task number to delete: "))  # exception might occur
    except ValueError:
        print("Please enter a valid number!")
        delete_task()

    del tasks[task_number - 1]
    print("Task deleted successfully! \n")


# View all tasks in the list
def view_tasks():
    print("Tasks: ")
    for i, task in enumerate(tasks):
        print(
            # 1. Completed - Shop for groceries
            # 2. Incomplete - Take the dog for a walk
            "{0}. {1} - {2}".format(
                i + 1,
                "Completed" if task["completed"] else "Incomplete",
                task["name"],
            ),
        )


# Mark a task as completed
def mark_complete():
    task_number = None
    try:
        # code that may raise an exception
        task_number = int(input("Enter task number to mark as complete: "))  # exception might occur
    except ValueError:
        print("Please enter a valid number!")
        mark_complete()

    _task = tasks[task_number - 1]
    _task["completed"] = True
    print("Task marked as completed! \n")


# Main program loop
while True:
    display_options()
    choice = None
    try:
        # code that may raise an exception
        choice = int(input("Enter your choice: "))  # exception might occur
        print()
    except ValueError:
        print("Please enter a valid number!")
        display_options()

    if choice == 1:
        add_task()
    elif choice == 2:
        delete_task()
    elif choice == 3:
        view_tasks()
    elif choice == 4:
        mark_complete()
    elif choice == 5:
        print("Goodbye")
        break  # ending a program
    else:
        print("Invalid choice. Please try again! \n")
