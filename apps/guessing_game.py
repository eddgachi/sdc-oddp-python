# Write a program that generates a random number and
# asks the user to guess the number. The program should use
# control flow statements to check if the guess is correct,
# too low or too high, and give the user a hint until they
# correctly guess the number.

import random

print("Guess the number!")
print("The number is between 1 and 100")
print("You have 5 attempts")

random_number = random.randint(1, 100)
attempts = 0

while attempts < 5:
    guess = int(input("Enter your guess: "))
    if guess == random_number:
        print("You win!")
        break  # stops the while loop
    elif guess < random_number:
        print("Too low!")
    else:
        print("Too high!")

    attempts += 1

if attempts == 5:
    print("You lost. The number was ", random_number)
