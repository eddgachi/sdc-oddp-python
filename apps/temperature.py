# Write a program to take temperature in Celsius as input
# from the user and convert it to Fahrenheit using the formula:
# f = (9/5)c + 32, where c is the temperature in Celsius and
# f is the temperature in Fahrenheit.

# BODMAS (9/5) * 20 74.8739872479234


def celsius_to_fahrenheit(celsius):
    fahrenheit = (celsius * 9 / 5) + 32
    return fahrenheit


user_celsius = float(input("Enter temperature in celsius: "))
calculated_fahrenheit = celsius_to_fahrenheit(celsius=user_celsius)

print("Temperature in fahrenheit: {:.2f}".format(calculated_fahrenheit))
