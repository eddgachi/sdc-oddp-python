# Write a program that takes a student's marks as input
# and calculates the percentage. The program then assigns a
# grade based on the percentage and returns the result.


def calculate_grade(marks):  # function defined and parameter is defined
    if marks >= 90:
        return "A+"
    elif marks >= 80:
        return "A"
    elif marks >= 70:
        return "B"
    elif marks >= 60:
        return "C"
    elif marks >= 50:
        return "D"
    else:
        return "F"


# asking the user to enter the student's name
student_name = input("Enter student name: ")
# asking for the student's marks and convert it to an int
student_marks = int(input("Enter student marks: "))
# calling the calculate_grade function and
# passing in the student marks as a parameter to be used in the function
# what the calculate_grade function returns back to us is stored in the
# variable student_grade
student_grade = calculate_grade(marks=student_marks)


print("Student name:", student_name)
print("Student marks:", student_marks)
print("Student grade:", student_grade)
