# A simple contact management application that stores contact allows the user to add, edit, and delete contacts.

# C - create
# R - retrieve
# U - update
# D - delete


class Contact:
    def __init__(self, name, phone, email):
        self.name = name
        self.phone = phone
        self.email = email


# [Contact("John", "07000", "john@mail.com"), Contact("mary", "010012", "mary@mail.com"),]
class ContactManager:
    def __init__(self):
        self.contacts = []

    def add_contact(self, name, phone, email):
        new_contact = Contact(name, phone, email)
        self.contacts.append(new_contact)

    def delete_contact(self, name):
        for single_contact in self.contacts:
            if single_contact.name == name:
                self.contacts.remove(single_contact)

    def search_contact(self, name):
        for single_contact in self.contacts:
            if single_contact.name == name:
                return single_contact

    def display_contacts(self):
        for single_contact in self.contacts:
            print("Name: {}".format(single_contact.name))
            print("Phone: {}".format(single_contact.phone))
            print("Email: {}".format(single_contact.email))
            print()


# calling out class
manager = ContactManager()

# adding a new contact
manager.add_contact("John", "0712345678", "john@mail.com")
manager.add_contact("Mary", "0753870924", "mary@mail.com")

# displaying the contacts
manager.display_contacts()

# search for a contact
contact = manager.search_contact("John")
print("Contact Found: {}".format(contact.name))

# delete a contact
manager.delete_contact("John")
manager.display_contacts()
