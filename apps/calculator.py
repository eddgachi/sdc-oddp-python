# Write a program that takes two numbers
# and an operator as input from the user, performs the operation,
# and returns the result.

print("Simple Calculator")

num1 = float(input("Enter first number: "))
num2 = float(input("Enter second number: "))

op = input("Enter operator (+, -, *, /): ")

if op == "+":
    result = num1 + num2
    print("Result: " + str(result))
elif op == "-":
    result = num1 - num2
    print("Result: " + str(result))
elif op == "*":
    result = num1 * num2
    print("Result: " + str(result))
elif op == "/":
    result = num1 / num2
    print("Result: " + str(result))
else:
    print("Please enter a valid operator!")
