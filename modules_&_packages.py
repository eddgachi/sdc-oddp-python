# a module is a file containing Python definitions and statements that can
# be imported and used in another Python file.

# A package is a collection of related modules that are grouped together
# in a directory/folder.

# import math
# from math import sqrt

from intro_to_python import my_second_module

# import my_module
from my_module import add_numbers, subtract_numbers

# x = 16
# y = math.sqrt(x)
# print(y)

# a = 8
# b = sqrt(a)
# print(b)

a = 4
b = 7
c = add_numbers(x=a, y=b)
d = subtract_numbers(a, b)
print(c)
print(d)

e = my_second_module.multiply_numbers(a, b)
print(e)
