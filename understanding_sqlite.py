import sqlite3

conn = sqlite3.connect("example.db")

conn.execute(
    """
    CREATE TABLE tasks(
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL,
       description TEXT,
       completed BOOLEAN NOT NULL
    )
    """
)

conn.commit()

conn.execute(
    """
    INSERT INTO tasks (name, description, completed)
    VALUES (?, ?, ?)
    """,
    ("Task 1", "Description for task 1", False),
)

conn.commit()

cursor = conn.execute("SELECT * FROM tasks")

for row in cursor:
    print(row)

conn.execute(
    """
    UPDATE tasks 
    SET completed = ?
    WHERE id = ?
    """,
    (True, 1),
)

conn.commit()

conn.execute(
    """
    DELETE FROM tasks
    WHERE id = ?
    """,
    (1,),
)
