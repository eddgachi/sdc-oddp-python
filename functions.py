def function_name(parameter):
    # function body
    # statements
    return "[expression]"


def greet(name):
    print("Hello " + name + ". How are you today?")


greet("Edd")


def square_number(x):
    return x * x


number_squared = square_number(4)
print(square_number(7))
print(number_squared)


number_squared = square_number(4)
print(square_number(7))
print(number_squared)
