# syntax errors - occur when the parser detects an incorrect statement.
# exceptions - whenever syntactically correct Python code results in an error.
# ValueError -
# TypeError -
# ZeroDivisionError -

# convert price to an integer
price = "one thousand"
# int(price)

# try:
#     int(price)
# except:
#     print("An exception occurred")

# try:
# code that may raise an exception
#     int(price)
# except ValueError:
#     print("A ValueError exception occurred")

# get the discount price by dividing by 20
# discount_price = price / 20
# print(discount_price)

# try:
# code that may raise an exception
#     discount_price = price / 20
#     print(discount_price)
# except TypeError:
# print("A TypeError exception occurred")

# _number = 9 / 0
# print(_number)

try:
    # code that may raise an exception
    _number = 9 / 0
    print(_number)
except ZeroDivisionError:
    print("A ZeroDivisionError exception occurred")

# try:
#     # code that may raise an exception
#     discount_price = price / 20
#     print(discount_price)
# except:
#     # code that is executed if an exception is raised
#     print("An exception occurred!")
