# regex - patterns used to match text strings - johnnie
# character
# character classes
# [abc]: matches any one of the characters 'a', 'b', or 'c'.
# [a-z]: matches any one lowercase letter.
# [A-Z]: matches any one uppercase letter.
# [0-9]: matches any one digit.
# [^abc]: matches any character that is not 'a', 'b', or 'c'.
# quantifiers
# *: matches zero or more occurrences.
# +: matches one or more occurrences.
# ?: matches zero or one occurrence.
# {m}: matches exactly m occurrences.
# {m, n}: matches between m and n occurrences.
# anchors
# ^: matches the start of the string.
# $: matches the end of the string.
# \b: matches a word boundary.

import re

# match a string that starts with 'Hello'
# pattern = "^Hello"
# text = "Hello, world!"

# if re.match(pattern, text):
#     print("Match found")
# else:
#     print("Match not found")

# match a string that ends with 'World'
# pattern = "World$"
# text = "Hello, World!"
# if re.search(pattern, text):
#     print("Match found")
# else:
#     print("Match not found")

# match a string that contains one or more digits
# we use \d+ to match one or more digits in a string, and re.search() to find the first occurrence
# pattern = "\d+"
# text = "The price is $5.99"
# match = re.search(pattern, text)
# if match:
#     print(match.group())

# replace all occurrences of 'red' with 'blue'
pattern = "red"
text = "The car is red and the shirt is red"
new_text = re.sub(pattern, "blue", text)
print(new_text)
