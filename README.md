# ODPP PYTHON

## Introduction to Python

Python is an interpreted high-level programming language that was first released in 1991. It is named after the famous British comedy troupe Monty Python's Flying Circus. Python is well-known for its readability and simplicity, making it an appealing choice for both novice and experienced developers.

Python is a multi-paradigm programming language that can be used to write object-oriented, procedural, and functional code. It is dynamically typed, which means that data types are determined at runtime, and it includes a large standard library that supports many common programming tasks like connecting to web servers, reading and writing files, and working with data.

In addition to its simplicity and readability, Python is also known for its large and active community of users and developers, as well as its versatility and ability to be used for a wide range of applications, including web development, scientific computing, data analysis, and machine learning.

## Why use Python

- Easy to learn and read: Python's syntax is straightforward, making it an ideal choice for beginners or those new to programming. The language is designed to be readable, meaning that code written in Python is often more concise and easier to understand than code written in other languages.
- Versatile and flexible: Python can be used for a wide range of applications, including web development, scientific computing, data analysis, artificial intelligence, and more. This versatility makes Python a great choice for a wide range of projects.

- Large and active community: Python has a large and active community of users and developers who contribute to the language, create packages and libraries, and provide support to others. This community provides a wealth of resources, including tutorials, forums, and tools, that can help you learn and use Python effectively.

- Rich standard library: Python comes with a large standard library that includes modules for many common programming tasks, such as connecting to web servers, reading and writing files, and working with data. This makes it easier to get started with Python, as you don't need to learn everything from scratch.

- Interoperability: Python is compatible with many other programming languages, including C and Java, making it easy to integrate with other systems and technologies.

In conclusion, Python is a popular choice for many various types of projects and is an excellent choice for both beginners and experienced developers due to its simplicity, versatility, and vast community.

## Disadvantages of Python

- **Performance:** Python is not as fast as some other programming languages, such as C or Java, due to its dynamic typing and interpreted nature.
- **Memory usage:** Python's memory usage can be higher than other programming languages, which can lead to slower performance on systems with limited memory.
- **Lack of low-level programming:** Python's high-level nature makes it difficult to do low-level programming, such as writing drivers or other system-level software.
- **Limited mobile support:** While Python can be used to create mobile apps, it is not as widely used for mobile app development as other programming languages, such as Java.

In conclusion, Python's advantages, including its simplicity, versatility, and large community, make it a popular choice for many different types of projects. However, its performance limitations and lack of low-level programming capabilities may make it less suitable for certain types of projects, such as high-performance system-level software or mobile app development.

## Variables and Data Types

**Variables** in Python are used to store values. They are containers that hold data, and they are assigned a name that you can use to reference the value. Variables in Python are dynamically typed, meaning that the type of the variable is determined at runtime, not at declaration.

#### There are several data types in Python:

- **Numbers:** Python supports integers, floating-point numbers, and complex numbers.
- **Strings:** Strings are sequences of characters and are enclosed in quotes. They can be single-quoted or double-quoted.
- **Lists:** Lists are ordered collections of elements, and they are denoted by square brackets. Lists are mutable, meaning that the elements in a list can be changed.
- **Tuples:** Tuples are similar to lists, but they are immutable, meaning that the elements in a tuple cannot be changed. Tuples are denoted by parentheses.
- **Dictionaries:** Dictionaries are unordered collections of key-value pairs, and they are denoted by curly braces.
- **Booleans:** Booleans represent the values True or False.
- **None:** None is a special constant in Python that represents the absence of a value.

### Numbers

- **Integers:** Positive or negative whole numbers without a fractional part. For example: 5, -10, 100, etc.
- **Floating-point numbers:** Numbers with a fractional part. For example: 3.14, 2.71, -0.5, etc.
- **Complex numbers:** Numbers with real and imaginary parts. The imaginary part is denoted by "j". For example: 3 + 4j, 2 + 6j, etc.

```python
x = 5  # x is my variable name and 5 is my number (int - whole number)
y = 10

z = 3.14  # floating-point number
d = 2.17

c = 3 + 4j  # complex number
d = 2 + 6j
```

You can perform arithmetic operations with numbers such as addition, subtraction, multiplication, division, modulo, etc.

```python
print(type(z))  # gives us the data type of a variable
print(int(z))  # converts other data types to integers
print(float(x)) # converts to a float
print(complex(z, x)) # converts to a complex number
print(x + y)
print(y - x)
print(x * y)
print(x / y)
print(x % y)
```

## Strings

In Python, a string is a sequence of characters that can be used to represent text or other data. Strings can be enclosed in either single quotes (') or double quotes (").

```python
name = "Alice"  # string
location = "Nairobi"
alice_data = name + " from " + location
string4 = "PYTHON"
string5 = "programming"
```

You can perform various operations on strings in Python, such as concatenation, repetition, and slicing.

You can also use various built-in functions to manipulate strings, such as len() to find the length of a string, str() to convert to string, lower() to convert to lowercase, upper() to convert to uppercase, etc.

```python
print(string4.lower())
print(string5.upper())
print(alice_data)
print(string4 * 4)
# slicing - upto but not including
print(string5[3:8])  # we count from zero
print(string4[0:2])
print(string5[3:])
print(string5[:4])
print(len(string5))
print(type(x))
print(str(x))
```

## Lists

In Python, a list is a collection of values that are ordered and changeable. Lists are used to store multiple items in a single variable. Lists are defined by square brackets [] and items are separated by commas.

```python
numbers_list = [71, 32, 83, 14, 900]
fruits_list = ["apple", "banana", "melon", "orange"]
list_data = [80, 5.67, "Hello"]
```

## Input and Output

In Python, input from the user is captured using the input() function. The function waits for the user to enter a string of characters and returns the input as a string. For example, consider the following code:

```python
name = input("Enter your name: ")
print("Hello, " + name)
```

The input() function prompts the user to enter their name, and the result is stored in the name variable. The second line uses the **print()** function to display a message to the screen, which includes the value of the **name** variable.

To format the output for readability, you can use various techniques, such as:

- Adding spaces and newlines
- Aligning text
- Using string concatenation to join multiple strings
- Using string formatting to insert values into a string

  Here's an example of using string formatting to create a formatted output:

```python
name = "John Doe"
age = 30
print("Name: {}\nAge: {}".format(name, age))
```

The **format()** method of strings allows you to insert values into a string. The curly braces **{}** act as placeholders for the values you want to insert. The **format()** method takes one or more arguments, which are the values to be inserted into the string. The \n character is used to insert a newline. The output of the above code would be:

```bash
Name: John Doe
Age: 30
```

## Operators and Expressions

Operators and expressions are an integral part of any programming language and are used to perform operations and manipulate data.

Python supports the following basic arithmetic operations:

- Addition: +
- Subtraction: -
- Multiplication: \*
- Division: /
- Integer Division: //
- Modulo (remainder of a division): %
- Exponentiation: \*\*

Here's an example of basic arithmetic operations:

```python
# Adding two numbers
print(2 + 3) # Output: 5

# Subtracting two numbers
print(5 - 2) # Output: 3

# Multiplying two numbers
print(2 * 3) # Output: 6

# Dividing two numbers
print(6 / 2) # Output: 3.0

# Integer Division
print(6 // 2) # Output: 3

# Modulo
print(7 % 3) # Output: 1

# Exponentiation
print(2 ** 3) # Output: 8
```

Comparison operators are used to compare two values and return a boolean value (**True** or **False**).
The following comparison operators are supported in Python:

- Equal to: ==
- Not equal to: !=
- Less than: <
- Less than or equal to: <=
- Greater than: >
- Greater than or equal to: >=

Here's an example of comparison operators:

```python
# Equal to
print(2 == 2) # Output: True

# Not equal to
print(2 != 3) # Output: True

# Less than
print(2 < 3) # Output: True

# Less than or equal to
print(2 <= 2) # Output: True

# Greater than
print(3 > 2) # Output: True

# Greater than or equal to
print(3 >= 3) # Output: True
```

Logical operators are used to combine boolean values and return a boolean value.
The following logical operators are supported in Python:

- **and**: returns **True** if both operands are True, False otherwise.
- **or**: returns **True** if at least one operand is True, False otherwise.
- **not**: returns **True** if the operand is False, False otherwise.

Here's an example of logical operators:

```python
# and
print(True and True) # Output: True
print(True and False) # Output: False

# or
print(True or False) # Output: True
print(False or False) # Output: False

# not
print(not True) # Output: False
print(not False) # Output: True
```

Operator precedence determines the order in which operations are performed in an expression.
In Python, operations are performed in the following order of precedence, from highest to lowest:

- Parentheses **()**
- Exponentiation **\*\***
- Multiplication and Division **\*/**
- Addition and Subtraction **+-**
- Comparison **<, >, <=, >=, ==, !=**
- Logical **not**
- Logical **and**
- Logical **or**

You can use parentheses to control the order of operations:

## Control Flow Statements

Control flow statements are used to control the flow of execution of a program. In Python, there are three main types of control flow statements:

### 1. if-elif-else statements:

The **if** statement allows you to test conditions and execute code blocks based on the results. The optional elif clause lets you test multiple conditions, and the else clause lets you specify a block of code to be executed if all conditions are False. Here's an example:

```python
x = 10
if x > 0:
    print("x is positive")
elif x == 0:
    print("x is zero")
else:
    print("x is negative")
```

Output:

```python
x is positive
```

### 2. for loops:

A **for** loop is used to iterate over a sequence (such as a list, tuple, or string) and execute a block of code for each item in the sequence. Here's an example:

```python
fruits = ["apple", "banana", "cherry"]
for fruit in fruits:
    print(fruit)
```

Output:

```python
apple
banana
cherry
```

### 3. while loops:

A **while** loop allows you to repeat a block of code as long as a certain condition is True. Here's an example:

```python
count = 1
while count <= 5:
    print(count)
    count += 1
```

Output:

```python
1
2
3
4
5
```

It's important to understand indentation and blocks in Python, as indentation is used to define the scope of code blocks. For example, the code inside a for loop or an if statement is indented, indicating that it is part of the loop or conditional statement.

## Lists and Dictionaries

Lists and dictionaries are data structures in Python used to store collections of items.

### A. Creating and using lists

Lists are created using square brackets [] and items are separated by commas.

```python
fruits = ['apple', 'banana', 'cherry']
print(fruits)
```

Output: **['apple', 'banana', 'cherry']**

### B. Indexing and slicing lists

Lists are zero-indexed, which means the first item in the list has an index of 0.

Slicing is used to extract a portion of a list.

```python
fruits = ['apple', 'banana', 'cherry', 'dates', 'elderberries']
print(fruits[0]) # first item
print(fruits[1:3]) # second to third item
print(fruits[-1]) # last item
```

Output:

```python
apple
['banana', 'cherry']
elderberries
```

### C. Modifying lists

Lists can be modified by adding or removing items.

```python
fruits = ['apple', 'banana', 'cherry']
fruits.append('dates') # adding item
fruits.remove('banana') # removing item
print(fruits)
```

Output: **['apple', 'cherry', 'dates']**

### D. Understanding dictionaries and their uses

Dictionaries are created using curly braces **{}** and key-value pairs are separated by colons.

Dictionaries are used to store key-value pairs, where each key is unique and is used to retrieve its associated value.

```python
person = {'name': 'John Doe', 'age': 32, 'address': '123 Main St.'}
print(person['name']) # retrieving value by key
person['age'] = 33 # modifying value
print(person)
```

Output:

```python
John Doe
{'name': 'John Doe', 'age': 33, 'address': '123 Main St.'}
```

## Functions

Functions are a fundamental concept in Python and provide a way to encapsulate and reuse code. Here's how you can define and call a function:

```python
def greet(name):
    print("Hello, " + name + "!")

greet("John")
```

In this example, the **greet** function takes a single parameter, **name**, and prints a message using that parameter. The function is defined using the **def** keyword, followed by the function name and a set of parentheses that contain any parameters. The code inside the function is indented, and ends with a colon (:).

To call the function, simply write the function name followed by any parameters you want to pass to the function in parentheses.

Regarding parameters and return values, a function can take any number of parameters and return any type of value. For example:

```python
def add(a, b):
    return a + b

result = add(2, 3)
print(result)
```

In this example, the **add** function takes two parameters, **a** and **b**, and returns their sum. The **return** keyword is used to specify the value that the function should return. To receive the return value, you assign the result of the function call to a variable, as shown above.

Using functions to modularize code means breaking your code into smaller, reusable pieces. This makes your code easier to read, test, and maintain. For example:

```python
def is_even(num):
    return num % 2 == 0

def print_even_numbers(numbers):
    for num in numbers:
        if is_even(num):
            print(num)

print_even_numbers([1, 2, 3, 4, 5])
```

In this example, we have two functions: **is_even** and **print_even_numbers**. The **is_even** function takes a single parameter, **num**, and returns **True** if the number is even, and **False** otherwise. The **print_even_numbers** function takes a list of numbers and uses the **is_even** function to determine whether each number is even, and if so, it prints the number. By breaking this code into two functions, we have made it easier to reuse the **is_even** function in other parts of our code, and made the **print_even_numbers** function easier to understand.

### Scope

n Python, the term "scope" refers to the region of a program where a variable or function can be accessed or referenced. In other words, scope determines where in a program a certain variable or function is available for use. There are two types of scope in Python: global scope and local scope.

Global scope refers to variables or functions that are defined at the top level of a program, outside of any functions or classes. These variables or functions can be accessed from any part of the program, including within functions or classes.

Local scope, on the other hand, refers to variables or functions that are defined within a specific function or class. These variables or functions are only accessible within that function or class, and cannot be accessed from outside of it.

When a variable or function is referenced within a program, Python first looks for it in the local scope, and if it's not found, it then looks for it in the global scope. If the variable or function is not found in either scope, Python will raise a NameError.

It's important to be aware of scope in Python when writing programs, to avoid conflicts or unexpected behavior. It's generally good practice to define variables and functions within the scope where they will be used, rather than relying on global variables or functions.

```python
name = "John Doe"  # global scope - outside any functions or classes

def some_function():
    age = 23  # local scope - inside a function or a class
    print(name + " is " + str(age))

```

### Modules and Packages

In Python, a module is a file containing Python definitions and statements that can be imported and used in another Python file. A package is a collection of related modules that are grouped together in a directory.

To use a module, you first need to **import** it into your code using the import statement, followed by the name of the module. For example, to use the built-in **math** module, you can import it like this:

```python
import math
```

Once you have imported a module, you can use its functions, classes, and other attributes by prefixing them with the module name, separated by a dot. For example, to use the sqrt function from the math module to calculate the square root of a number, you can do:

```python
import math

x = 16
y = math.sqrt(x)

print(y)
```

You can also import specific functions or classes from a module using the from statement. For example:

```python
from math import sqrt

x = 16
y = sqrt(x)

print(y)  # output: 4.0
```

To create a custom module, you can simply write a Python file with the code you want to include and save it with a **.py** extension. You can then import and use the functions, classes, and other attributes defined in your custom module from another Python file.

To create a package, you can group related modules in a directory with an **init.py** file that tells Python that the directory should be treated as a package. You can then use the package name followed by the module name to import specific modules from the package. For example:

```python
import mypackage.mymodule
```

You can also use the **from** statement to import specific functions or classes from a module in a package. For example:

```python
Name: John Doe
Age: 30
```

## Object-Oriented Programming

Object-oriented programming (OOP) is a programming paradigm that uses the concept of "objects" to structure code. Python is an object-oriented language, which means that it provides support for OOP concepts like classes, objects, inheritance, and polymorphism.

**Classes and Objects**: A class is a blueprint or template for creating objects, which are instances of the class. Classes define attributes (data) and methods (functions) that are common to all objects of that class.

Here is an example of defining and using a class:

```python
class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say_hello(self):
        print(f"Hello, my name is {self.name} and I'm {self.age} years old.")

person = Person("Alice", 25)
person.say_hello()  # Output: Hello, my name is Alice and I'm 25 years old.
```

In this example, we define a **Person** class with two attributes (**name** and **age**) and one method (**say_hello**). We create an instance of the **Person** class and call the **say_hello** method to print a greeting.

**Inheritance**: Inheritance allows you to create a new class based on an existing class, inheriting the attributes and methods of the parent class. This can be useful for creating more specialized classes without duplicating code.

Here is an example of a class hierarchy that uses inheritance:

```python
class Animal:
    def __init__(self, name):
        self.name = name

    def make_sound(self):
        pass

class Dog(Animal):
    def make_sound(self):
        print("Woof!")

class Cat(Animal):
    def make_sound(self):
        print("Meow!")

dog = Dog("Fido")
cat = Cat("Whiskers")
dog.make_sound()  # Output: Woof!
cat.make_sound()  # Output: Meow!
```

In this example, we define an **Animal** class with a **name** attribute and a **make_sound** method. We then create two child classes (**Dog** and **Cat**) that inherit from the **Animal** class and override the **make_sound** method.

**Polymorphism**: Polymorphism allows you to use objects of different classes in the same way, as long as they have a common interface (i.e. the same methods with the same arguments). This can be useful for writing flexible code that can work with different types of objects.

Here is an example of polymorphism using the **make_sound** method from the previous example:

```python
animals = [Dog("Fido"), Cat("Whiskers")]

for animal in animals:
    animal.make_sound()
```

In this example, we create a list of **Animal** objects that includes a **Dog** and a **Cat**. We then loop over the list and call the **make_sound** method on each object, which prints out the appropriate sound for each type of animal. This works because both the **Dog** and **Cat** classes have a **make_sound** method that takes no arguments.

## Advanced Topics

### File Input/Output

File input/output in Python is the process of working with files to read or write data from or to them.

To read data from a file, you can use the **open()** function to open a file and then read data using the **read()** or **readline()** method.

For example, the following code opens a file called **example.txt** and reads the contents of the file:

```python
with open('example.txt', 'r') as f:
    data = f.read()
```

To write data to a file, you can use the **write()** method to write data to a file.

For example, the following code creates a new file called **output.txt** and writes some data to it:

```python
with open('output.txt', 'w') as f:
    f.write('Hello, world!')
```

You can also use the **append** mode **'a'** to add content to the end of an existing file, without erasing it.

```python
with open('output.txt', 'a') as f:
    f.write('How are you today?')
```

When working with files, it is important to close the file when you are finished working with it. To avoid forgetting to close the file, it is best to use a **with** statement to open the file, which automatically closes it when you are done.

### Exception Handling

Exception handling is a mechanism in Python to handle errors that occur during the execution of a program. When an error occurs, an exception is raised, which stops the program's execution. Exception handling provides a way to catch these exceptions and handle them gracefully, preventing the program from crashing.

In Python, the try-except block is used for exception handling. The try block contains the code that might raise an exception, and the except block contains the code that handles the exception. When an exception is raised in the try block, the code in the except block is executed, and the program continues to run.

Here is the syntax of a try-except block:

```python
try:
    # Code that might raise an exception
except ExceptionType:
    # Code to handle the exception
```

In this example, **ExceptionType** is the type of exception that might be raised. It can be a built-in exception such as **ValueError** or **TypeError**, or a custom exception defined by the programmer.

The except block can handle different types of exceptions by using multiple except clauses:

```python
try:
    # Code that might raise an exception
except ValueError:
    # Code to handle a ValueError exception
except TypeError:
    # Code to handle a TypeError exception
```

It's also possible to have a single except block that handles multiple exception types:

```python
try:
    # Code that might raise an exception
except (ValueError, TypeError):
    # Code to handle a ValueError or TypeError exception
```

In addition to handling exceptions, it's often useful to raise exceptions to signal errors in the program. To raise an exception, use the **raise** keyword followed by the type of exception:

```python
raise ValueError("Invalid value")
```

Finally, the try-except block can have a **finally** clause that contains code that is executed regardless of whether an exception was raised:

```python
try:
    # Code that might raise an exception
except ValueError:
    # Code to handle a ValueError exception
finally:
    # Code that is always executed
```

In summary, exception handling is a critical part of writing robust and error-free Python code. It provides a way to handle errors gracefully, preventing programs from crashing and providing a better user experience.

### Making HTTP Requests

Python Requests module is a popular HTTP client library that simplifies making HTTP requests in Python. It allows developers to send HTTP requests to a server and receive a response in a format that is easy to work with. In this context, HTTP (Hypertext Transfer Protocol) is a protocol used for transmitting data over the internet.

The requests module allows you to send different types of requests such as GET, POST, PUT, DELETE, etc. to the server. It also allows you to add headers, parameters, and authentication to your requests.

Here are some of the important concepts to understand when working with the requests module:

1. Making a GET Request: You can make a GET request using the requests.get() method. It takes a URL as an argument and returns a response object.

```python
import requests

response = requests.get('https://jsonplaceholder.typicode.com/posts')

print(response.content)
```

2. HTTP Response Status Codes: HTTP response status codes indicate whether a request has been successfully completed or not. Requests module returns the HTTP status code along with the response object.

```python
import requests

response = requests.get('https://jsonplaceholder.typicode.com/posts')

if response.status_code == 200:
    print('Request was successful')
else:
    print('Request was unsuccessful')
```

3. Handling Exceptions: When working with HTTP requests, errors can occur due to various reasons such as server errors, network errors, and so on. The requests module provides different types of exceptions that you can use to handle these errors.

```python
import requests

try:
    response = requests.get('https://jsonplaceholder.typicode.com/posts')
    response.raise_for_status()
except requests.exceptions.HTTPError as err:
    print(err)
```

4. HTTP Headers: HTTP headers contain additional information about a request or a response. You can add headers to your requests using the headers parameter.

```python
import requests

headers = {'Content-Type': 'application/json'}

response = requests.get('https://jsonplaceholder.typicode.com/posts', headers=headers)

print(response.content)
```

5. HTTP Authentication: You can authenticate your HTTP requests using different authentication methods such as Basic Authentication, Digest Authentication, and OAuth. The requests module provides support for various authentication methods.

```python
import requests

response = requests.get('https://api.github.com/user', auth=('username', 'password'))

print(response.content)
```

In summary, the requests module is a powerful tool for making HTTP requests in Python. By understanding the different concepts discussed above, you can use this module to interact with different web services and APIs on the internet.

### Regular Expressions

Regular expressions (regex) are patterns used to match text strings. They are a powerful tool in text processing, search and replace operations, and input validation. Python provides a built-in module called **re** for working with regular expressions.

Here are some important concepts to understand about regular expressions:

1. Characters: Any character in a regular expression matches itself, except for special characters like **., |, \*, +, ?, (, ), [, ], {, }, \,** and **^**.

2. Character classes: Character classes match any one of a group of characters. The most common character classes are:

- **[abc]**: matches any one of the characters 'a', 'b', or 'c'.
- **[a-z]**: matches any one lowercase letter.
- **[A-Z]**: matches any one uppercase letter.
- **[0-9]**: matches any one digit.
- **[^abc]**: matches any character that is not 'a', 'b', or 'c'.

3. Quantifiers: Quantifiers specify how many times a character or group of characters should be matched. The most common quantifiers are:

- **\***: matches zero or more occurrences.
- **+**: matches one or more occurrences.
- **?**: matches zero or one occurrence.
- **{m}**: matches exactly m occurrences.
- **{m, n}**: matches between m and n occurrences.

4. Anchors: Anchors specify where in the string a match should occur. The most common anchors are:

- **^**: matches the start of the string.
- **$**: matches the end of the string.
- **\b**: matches a word boundary.

Now, let's see some code examples:

```python
import re

# match a string that starts with 'Hello'
pattern = '^Hello'
text = 'Hello, World!'
if re.match(pattern, text):
    print('Match found')
else:
    print('Match not found')

# match a string that ends with 'World'
pattern = 'World$'
text = 'Hello, World!'
if re.search(pattern, text):
    print('Match found')
else:
    print('Match not found')

# match a string that contains one or more digits
pattern = '\d+'
text = 'The price is $5.99'
match = re.search(pattern, text)
if match:
    print(match.group())

# replace all occurrences of 'red' with 'blue'
pattern = 'red'
text = 'The car is red and the shirt is red'
new_text = re.sub(pattern, 'blue', text)
print(new_text)
```

In the first example, we use **re.match()** to check if the text starts with 'Hello'. In the second example, we use **re.search()** to check if the text ends with 'World'. In the third example, we use **\d+** to match one or more digits in a string, and **re.search()** to find the first occurrence. In the fourth example, we use **re.sub()** to replace all occurrences of 'red' with 'blue' in a string.

Regular expressions can be quite complex, but they are a powerful tool for text processing and manipulation. It's important to test your regular expressions thoroughly and make sure they are working as expected.
