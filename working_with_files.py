# read data - open the file, read the contents
# writing data - open the file, write data to the file


# with open("course.txt", "r") as f:
#     data = f.read()
#     print(data)

with open("output.txt", "w") as f:
    f.write("Hello, world! \nThis is an example of writing to a file.")

with open("output.txt", "a") as f:
    f.write("\nThis is more content.")
