# scope - determines where in a program a certain variable or
# function is available for use

# global -  variables or functions that are defined at the top
# level of a program, outside of any functions or classes

# local -refers to variables or functions that are
# defined within a specific function or class

name = "John Doe"  # global scope - outside any functions or classes


def login_user():
    is_logged_in = False
    return is_logged_in


def register_user():
    pass


def some_function():
    age = 23  # local scope - inside a function or a class
    print(name + " is " + str(age))


login_user()
# print(age)
