# Object-oriented programming (OOP) is a programming paradigm that uses
# the concept of "objects" to structure code.

# OOP concepts like classes, objects, inheritance, and polymorphism.
#  A class is a blueprint or template for creating objects
# Classes define attributes (data) and methods (functions)
# that are common to all objects of that class.


class Person:
    is_kenyan = False

    # self - a reference to the current instance of the class,
    # and is used to access variables that belongs to the class
    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender

    def say_hello(self):
        print("Hello, my name is {} and I am {} years old".format(self.gender, self.age))


person = Person("Alice", 27)
person.say_hello()


class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def bark(self):
        print("{} says woff!".format(self.name))


dog = Dog("Bosco", 7)
# print(dog.name)
# print(dog.age)
dog.bark()

dog2 = Dog("John", 2)
# print(dog2.name)
dog2.bark()
